package com.manthalabs.microservicesample1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationController {

    @GetMapping("/user")
    public String getApplicationInfo(){
        return "user1";
    }
}
